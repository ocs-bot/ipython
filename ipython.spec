%bcond_with sphinx
%bcond_with notebook

Summary:        An enhanced interactive Python shell
Name:           ipython
Version:        8.17.2
Release:        1%{?dist}
License:        (BSD and MIT and Python) and GPLv2+
URL:            http://ipython.org/
Source0:        %pypi_source

BuildArch:      noarch
BuildRequires:  make python3-devel python3-setuptools python3-stack-data

%description
IPython provides a rich architecture for interactive computing with:

 - A powerful interactive shell.
 - A kernel for Jupyter.
 - Support for interactive data visualization and use of GUI toolkits.
 - Flexible, embeddable interpreters to load into your own projects.
 - Easy to use, high performance tools for parallel computing.

%package -n python3-ipython
Summary:        An enhanced interactive Python shell
BuildRequires:  python3-backcall python3-decorator python3-jedi >= 0.10
BuildRequires:  python3-pexpect python3-pickleshare python3-prompt-toolkit >= 2
BuildRequires:  python3-traitlets >= 4.2

Requires:       (tex(amsmath.sty) if /usr/bin/dvipng)
Requires:       (tex(amssymb.sty) if /usr/bin/dvipng)
Requires:       (tex(amsthm.sty)  if /usr/bin/dvipng)
Requires:       (tex(bm.sty)      if /usr/bin/dvipng)

Provides:       ipython3 = %{version}-%{release}
Provides:       ipython = %{version}-%{release}
Provides:       python3-ipython-console = %{version}-%{release}

%description -n python3-ipython
This package provides IPython for in a terminal.

%if %{with notebook}
%package -n python3-ipython+notebook
Summary:    Metapackage for python3-ipython: notebook extras

%description -npython3-ipython+notebook
This is a metapackage bringing in notebook extras requires for python3-ipython.
It makes sure the dependencies are installed.
%endif

%if %{with sphinx}
%package -n python3-ipython-sphinx
Summary:        Sphinx directive to support embedded IPython code
BuildRequires:  python3-sphinx
Requires:       python3-ipython = %{version}-%{release}
Requires:       python3-sphinx

%description -n python3-ipython-sphinx
This package contains the ipython sphinx extension.
%endif

%prep
%autosetup -p1

sed -i '1d' $(grep -lr '^#!/usr/' IPython)
find . -name '*.py' -print0 | xargs -0 sed -i '1s|^#!python|#!%{__python3}|'


%build
%py3_build


%install
%py3_install

rm -r %{buildroot}%{python3_sitelib}/IPython/*/tests

%if %{without sphinx}
rm -rf %{buildroot}%{python3_sitelib}/IPython/sphinxext/
%endif


%files -n python3-ipython
%{_bindir}/ipython3
%{_bindir}/ipython
%{_mandir}/man1/ipython.*

%dir %{python3_sitelib}/IPython
%{python3_sitelib}/IPython/external
%{python3_sitelib}/IPython/__pycache__/
%{python3_sitelib}/IPython/*.py*
%{python3_sitelib}/IPython/py.typed
%dir %{python3_sitelib}/IPython/testing
%{python3_sitelib}/IPython/testing/__pycache__/
%{python3_sitelib}/IPython/testing/*.py*
%{python3_sitelib}/IPython/testing/plugin
%{python3_sitelib}/ipython-%{version}-py%{python3_version}.egg-info/

%{python3_sitelib}/IPython/core/
%{python3_sitelib}/IPython/extensions/
%{python3_sitelib}/IPython/lib/
%{python3_sitelib}/IPython/terminal/
%{python3_sitelib}/IPython/utils/

%if %{with notebook}
%files -n python3-ipython+notebook
%{python3_sitelib}/*.egg-info
%endif

%if %{with sphinx}
%files -n python3-ipython-sphinx
%{python3_sitelib}/IPython/sphinxext/
%endif

%changelog
* Fri Nov 10 2023 Upgrade Robot <upbot@opencloudos.org> - 8.17.2-1
- Upgrade to version 8.17.2

* Thu Sep 28 2023 Wang Guodong <gordonwwang@tencent.com> - 8.15.0-1
- Upgrade to version 8.15.0

* Mon Sep 25 2023 Miaojun Dong <zoedong@tencent.com> - 8.6.0-6
- Rebuild for python-traitlets-5.10.0

* Wed Sep 20 2023 cunshunxia <cunshunxia@tencent.com> - 8.6.0-5
- Rebuilt for python 3.11, after python-stack-data.

* Tue Sep 19 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 8.6.0-4
- Rebuilt for python 3.11

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 8.6.0-3
- Rebuilt for OpenCloudOS Stream 23.09

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 8.6.0-2
- Rebuilt for OpenCloudOS Stream 23.05

* Wed Nov 30 2022 cunshunxia <cunshunxia@tencent.com> - 8.6.0-1
- initial build
